﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
	class Program
	{
		static void Main(string[] args)
		{
			TextWriter tw = new StreamWriter("log.txt") { AutoFlush = true };

			using (MemoryMappedFile mmFile = MemoryMappedFile.OpenExisting("Demo"))
			using (MemoryMappedViewAccessor accessor = mmFile.CreateViewAccessor())
			{
				string rxID;
				string txID;

				byte[] txIdBytes = new byte[accessor.ReadInt32(0)];
				accessor.ReadArray(4, txIdBytes, 0, txIdBytes.Length);

				byte[] rxIdBytes = new byte[accessor.ReadInt32(4 + txIdBytes.Length)];
				accessor.ReadArray(4 + 4 + txIdBytes.Length, rxIdBytes, 0, rxIdBytes.Length);

				rxID = Encoding.UTF8.GetString(txIdBytes); //reverse these!!
				txID = Encoding.UTF8.GetString(rxIdBytes);

				using (AnonymousPipeClientStream tx = new AnonymousPipeClientStream(PipeDirection.Out, txID))
				using (AnonymousPipeClientStream rx = new AnonymousPipeClientStream(PipeDirection.In, rxID))
				{
					while (true)
					{
						int result = rx.ReadByte();
						tw.WriteLine(result);
						if (result == 102)
							tx.WriteByte(104);
					}
				}
			}
		}
	}
}
