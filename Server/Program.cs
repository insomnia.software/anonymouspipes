﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
	class Program
	{
		static void Main(string[] args)
		{
			using (MemoryMappedFile mmFile = MemoryMappedFile.CreateNew("Demo", 1024))
			using (MemoryMappedViewAccessor accessor = mmFile.CreateViewAccessor())
			{
				using (AnonymousPipeServerStream tx = new AnonymousPipeServerStream(PipeDirection.Out, HandleInheritability.Inheritable))
				using (AnonymousPipeServerStream rx = new AnonymousPipeServerStream(PipeDirection.In, HandleInheritability.Inheritable))
				{
					string txID = tx.GetClientHandleAsString();
					string rxID = rx.GetClientHandleAsString();

					Console.WriteLine(txID);
					Console.WriteLine(rxID);

					//put ids in shared memory
					byte[] txIdBytes = Encoding.UTF8.GetBytes(txID);
					byte[] rxIdBytes = Encoding.UTF8.GetBytes(rxID);

					accessor.Write(0, txIdBytes.Length);
					accessor.WriteArray(4, txIdBytes, 0, txIdBytes.Length);

					accessor.Write(4 + txIdBytes.Length, rxIdBytes.Length);
					accessor.WriteArray(4 + 4 + txIdBytes.Length, rxIdBytes, 0, rxIdBytes.Length);

					ProcessStartInfo startInfo = new ProcessStartInfo(@"Client.exe");
					startInfo.UseShellExecute = false;
					Process p = Process.Start(startInfo);

					tx.WriteByte(100);
					Thread.Sleep(1000);
					tx.WriteByte(101);
					Thread.Sleep(1000);
					tx.WriteByte(102);
					Console.WriteLine("Done");

					while (true)
					{
						Console.WriteLine(rx.ReadByte());
					}

					p.WaitForExit();
					Console.ReadLine();
				}
			}
		}
	}
}
